package com.example.network;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {

        @GET("SuperApp_Listing_feed")
        Call<String> getResult();
    }