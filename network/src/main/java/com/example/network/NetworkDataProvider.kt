package com.example.network


import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkDataProvider() {

    companion object {
        const val url = "https://raw.githubusercontent.com/santoshroyal/SuperAppFeed/master/"
    }

    fun fetch(responseCallback: ApiResponseCallback, isNetworkAvailable: Boolean) {
        if (isNetworkAvailable) {
            val service = getRetrofitInstance().create(Api::class.java)
            val call = service.result
            call.enqueue(object : Callback<String> {
                override fun onResponse(
                    call: Call<String>,
                    response: Response<String>
                ) {
                    onResponseRecived(response, responseCallback)
                }
                override fun onFailure(call: Call<String>, t: Throwable) {
                    responseCallback.onError("Unknown Error")
                }
            })
        } else {
            responseCallback.onError("Network Error")
        }
    }

    private fun onResponseRecived(
        response: Response<String>, responseCallback: ApiResponseCallback
    ) {
        if (response.code() == 200)
            responseCallback.onSuccess(response.body().toString())
        else
            responseCallback.onError("\"Unknown Error\"")
    }

    private fun getRetrofitInstance(): Retrofit {
        val gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder().baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create(gson)).build()
    }

}
