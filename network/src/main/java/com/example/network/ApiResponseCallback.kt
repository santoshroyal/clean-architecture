package com.example.network

interface ApiResponseCallback {

   fun onSuccess(response: String)
    fun onError(errorType : String)
}