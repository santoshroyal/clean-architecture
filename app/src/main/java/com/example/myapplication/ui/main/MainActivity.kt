package com.example.myapplication.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.myapplication.R
import com.example.myapplication.controllers.ApiRequestController
import com.example.myapplication.databinding.MainActivityBinding
import com.example.myapplication.entities.ApiEntity
import com.example.myapplication.framework.gateways.ApiStatusRequestGateway
import com.example.myapplication.interactors.ApiInteractor
import com.example.myapplication.presenter.ApiStatusPresenter
import com.example.myapplication.ui.main.viewdata.ApiStatusViewData


class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityMainBinding: MainActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.main_activity)

        //Todo: This part need to be executed with Dependency Injection
        val viewData = ApiStatusViewData()
        val presenter = ApiStatusPresenter(viewData)
        val statusCallback = ApiStatusRequestGateway()
        val entity = ApiEntity("initial status")
        val interactor = ApiInteractor(statusCallback, entity)
        val controller = ApiRequestController(presenter, interactor)
        activityMainBinding.viewData = viewData
        activityMainBinding.controller = controller
    }
}
