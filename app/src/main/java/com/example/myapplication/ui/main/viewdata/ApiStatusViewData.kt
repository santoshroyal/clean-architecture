package com.example.myapplication.ui.main.viewdata


import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR


class ApiStatusViewData : BaseObservable() {

     val GONE : Int = 1
     val VISIBLE : Int = 0

    @Bindable
    private var statusMessage: String? = null

    @Bindable
    private var requestButtonName: String = "Fetch"

    @Bindable
    private var resetButtonName: String = "Reset"

    @Bindable
    private var requestButtonVisibilityStatus: Int? = 0

    @Bindable
    private var resetButtonVisibilityStatus: Int? = 1

    @Bindable
    private var progressBarVisibilityStatus: Int? = 1

    @Bindable
    private var messageTextViewVisibilityStatus: Int? = 1

    @Bindable
    fun getRequestButtonVisibilityStatus(): Int? {
        return requestButtonVisibilityStatus;
    }

    fun setRequestButtonVisibilityStatus(visibility : Int) {
        requestButtonVisibilityStatus = visibility
        notifyPropertyChanged(BR.requestButtonVisibilityStatus);
    }

    @Bindable
    fun getResetButtonVisibilityStatus(): Int? {
        return resetButtonVisibilityStatus;
    }

    fun setResetButtonVisibilityStatus(visibility : Int) {
        resetButtonVisibilityStatus = visibility
        notifyPropertyChanged(BR.resetButtonVisibilityStatus);
    }

    @Bindable
    fun getProgressBarVisibilityStatus(): Int? {
        return progressBarVisibilityStatus;
    }

    fun setProgressBarVisibilityStatus(visibility : Int) {
        progressBarVisibilityStatus = visibility
        notifyPropertyChanged(BR.progressBarVisibilityStatus);
    }

    @Bindable
    fun getMessageTextViewVisibilityStatus(): Int? {
        return messageTextViewVisibilityStatus;
    }

    fun setMessageTextViewVisibilityStatus(visibility : Int) {
        messageTextViewVisibilityStatus = visibility
        notifyPropertyChanged(BR.messageTextViewVisibilityStatus);
    }

    @Bindable
    fun getStatusMessage(): String? {
        return statusMessage;
    }

    fun setStatusMessage(message: String) {
        statusMessage = message
        notifyPropertyChanged(BR.statusMessage);
    }

    @Bindable
    fun getRequestButtonName(): String {
        return requestButtonName;
    }

    @Bindable
    fun getResetButtonName(): String {
        return resetButtonName;
    }

}
