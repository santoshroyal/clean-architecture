package com.example.myapplication.callbacks

interface ApiStatusCallback{
    fun onStatusReceived(status : String)
}