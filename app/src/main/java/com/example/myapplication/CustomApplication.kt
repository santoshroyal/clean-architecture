package com.example.myapplication

import android.app.Application
import android.content.Context


class CustomApplication() : Application() {


    init {
        instance = this
    }

    companion object {
        private var instance: CustomApplication? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        val context: Context = CustomApplication.applicationContext()
    }

}