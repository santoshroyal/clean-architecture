package com.example.myapplication.framework.gateways

import com.example.myapplication.CustomApplication
import com.example.myapplication.interactors.Interfaces.ApiStatusContracts
import com.example.myapplication.callbacks.ApiStatusCallback
import com.example.myapplication.utils.isConnectedToNetwork
import com.example.network.ApiResponseCallback
import com.example.network.NetworkDataProvider

class ApiStatusRequestGateway :
    ApiStatusContracts {

    override fun getApiResponseStatus(apiStatus: ApiStatusCallback) {

        NetworkDataProvider().fetch(object : ApiResponseCallback {
            override fun onSuccess(response: String) {
                apiStatus.onStatusReceived("ok")
            }
            override fun onError(errorType: String) {
                apiStatus.onStatusReceived(errorType)
            }
        }, CustomApplication.applicationContext().isConnectedToNetwork())
    }


}