package com.example.myapplication.interactors.Interfaces

import com.example.myapplication.callbacks.ApiStatusCallback

interface ApiStatusContracts{
    fun getApiResponseStatus(apiStatus : ApiStatusCallback)
}