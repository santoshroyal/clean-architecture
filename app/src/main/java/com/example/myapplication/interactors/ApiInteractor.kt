package com.example.myapplication.interactors

import com.example.myapplication.interactors.Interfaces.ApiStatusContracts
import com.example.myapplication.callbacks.ApiStatusCallback
import com.example.myapplication.interactors.Interfaces.ApiEntityContracts


class ApiInteractor(
    val statusContracts: ApiStatusContracts,
    val entity: ApiEntityContracts
) {

    fun getApiStatus(callback : ApiStatusCallback) {
        statusContracts.getApiResponseStatus(object :
            ApiStatusCallback {
            override fun onStatusReceived(status: String) {
                callback.onStatusReceived(status)
                upadateEntity(status)
            }
        })
    }

    fun upadateEntity(status: String) {
      entity.setApiStatus(status)
    }
}
