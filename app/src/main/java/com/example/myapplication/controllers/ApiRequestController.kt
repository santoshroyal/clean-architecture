package com.example.myapplication.controllers

import androidx.databinding.BaseObservable
import com.example.myapplication.interactors.ApiInteractor
import com.example.myapplication.presenter.ApiStatusPresenter
import com.example.myapplication.callbacks.ApiStatusCallback


class ApiRequestController(val presenter: ApiStatusPresenter, val interactor: ApiInteractor) :
    BaseObservable() {


    fun onRequestButtonClicked() {
        presenter.requestInvoked()
        requestApiStatus()
    }


    fun onResetButtonClicked() {
        presenter.resetRequested()
    }

    private fun requestApiStatus() {
        interactor.getApiStatus(object :
            ApiStatusCallback {
            override fun onStatusReceived(status: String) {
                onApiStatusReceived(status)
            }
        })
    }

    private fun onApiStatusReceived(status: String) {
        presenter.apiStatusReceived(status)
    }


}