package com.example.myapplication.utils

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.BindingAdapter

@BindingAdapter("visibleOrGone")
fun View.setVisibleOrGone(show: Int) {
    visibility = if (show == 0) VISIBLE else GONE
}