package com.example.myapplication.presenter

import android.text.TextUtils
import com.example.myapplication.ui.main.viewdata.ApiStatusViewData

class ApiStatusPresenter(val viewData: ApiStatusViewData) {
    private val apiSuccessStatus = "ok"
    private val noNetwokStatus = "Network Error"
    private val successMessage = "API call was successful"
    private val errorMessage = "API call was unsuccessful"
    private val noNetworkMessage = "No Network! Please check your connection"

    fun requestInvoked() {
        viewData.setRequestButtonVisibilityStatus(viewData.GONE)
        viewData.setProgressBarVisibilityStatus(viewData.VISIBLE)
    }

    fun apiStatusReceived(status: String) {
        setMessageOnApiStatus(status)
        visibilityChangeOnApiStatusReceived()
    }

    private fun setMessageOnApiStatus(status: String){
        if (!TextUtils.isEmpty(status) && apiSuccessStatus.equals(status, true)) {
            viewData.setStatusMessage(successMessage)
        }
        else if(!TextUtils.isEmpty(status) && noNetwokStatus.equals(status, true)) {
            viewData.setStatusMessage(noNetworkMessage)
        }
        else viewData.setStatusMessage(errorMessage)
    }

   private fun visibilityChangeOnApiStatusReceived(){
        viewData.setProgressBarVisibilityStatus(viewData.GONE)
        viewData.setMessageTextViewVisibilityStatus(viewData.VISIBLE)
        viewData.setResetButtonVisibilityStatus(viewData.VISIBLE)
    }

    fun resetRequested() {
        viewData.setRequestButtonVisibilityStatus(viewData.VISIBLE)
        viewData.setResetButtonVisibilityStatus(viewData.GONE)
        viewData.setMessageTextViewVisibilityStatus(viewData.GONE)
    }

}