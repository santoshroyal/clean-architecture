package com.example.myapplication.entities

import com.example.myapplication.interactors.Interfaces.ApiEntityContracts

data class ApiEntity(var status : String) : ApiEntityContracts {


    override fun setApiStatus(apiStatus: String) {
        status = apiStatus;
    }


}